import 'package:flutter/material.dart';

void main() {
  runApp(const FeatureA());
}

class FeatureA extends StatelessWidget {
  const FeatureA({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Feature A "),
        backgroundColor: Colors.green,
      ),
      body: Center(
        child: Container(
          padding: EdgeInsets.all(15),
          color: Colors.green,
          width: 400,
          height: 80,
          child: const Text(
            "FEATURE A",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontSize: 50,
            ),
          ),
        ),
      ),
    );
  }
}
